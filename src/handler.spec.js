import axios from 'axios';
import handler from './handler';

jest.mock('axios'); 

describe('test handler', () => {
  test('should return correct values for no dental care and 100% company payment', async () => {
    const noDentalCare100CompanyPayment = {
      data: {
        success: true,
        policy: {
          workers: [[
            { age: 67, childs: 4 },
            { age: 49, childs: 0 },
            { age: 25, childs: 2 },
            { age: 72, childs: 1 },
          ]],
          has_dental_care: false,
          company_percentage: 100,
        },
      },
    };
    axios.get.mockResolvedValue(noDentalCare100CompanyPayment);
    expect((await handler()).body).toEqual(JSON.stringify({
        employeeDetailsList: [
          { age: 67, childs: 4, policyCost: 0, companyPayment: 0, workerCopayment: 0 },
          { age: 49, childs: 0, policyCost: 0.279, companyPayment: 0.279, workerCopayment: 0 },
          { age: 25, childs: 2, policyCost: 0.5599, companyPayment: 0.5599, workerCopayment: 0 },
          { age: 72, childs: 1, policyCost: 0, companyPayment: 0, workerCopayment: 0 },
        ],
        totalPolicyCost: 0.8389,
        totalCompanyPayment: 0.8389,
        totalEmployeeCopayment: 0,
    }));
  });
  test('should return correct values for dental care and 70% company payment', async () => {
    const dentalCare70CompanyPayment = {
      data: {
        success: true,
        policy: {
          workers: [[
            { age: 49, childs: 0 },
            { age: 25, childs: 2 },
          ]],
          has_dental_care: true,
          company_percentage: 70,
        },
      },
    };
    axios.get.mockResolvedValue(dentalCare70CompanyPayment);
    expect((await handler()).body).toEqual(JSON.stringify({
      employeeDetailsList: [
        { age: 49, childs: 0, policyCost: 0.399, companyPayment: 0.2793, workerCopayment: 0.1197 },
        { age: 25, childs: 2, policyCost: 0.8079, companyPayment: 0.5655, workerCopayment: 0.2424 },
      ],
      totalPolicyCost: 1.2069,
      totalCompanyPayment: 0.8448,
      totalEmployeeCopayment: 0.3621,
    }));
  });
  
  test('should correcly handle no controlled failure', async () => {
    axios.get.mockRejectedValue({ error: 'there was an error' });
    const response = await handler();
    expect(response.statusCode).toEqual(500);
    expect(response.body).toEqual(JSON.stringify({ error: 'there was an error' }));
  });
  
  test('should correcly handle controlled failure', async () => {
    axios.get.mockResolvedValue({ data: { success: false } });
    const response = await handler();
    expect(response.statusCode).toEqual(500);
    expect(response.body).toEqual(JSON.stringify({
      error: 'data source recovery doesn\'t indicate success'
    }));
  });
});
