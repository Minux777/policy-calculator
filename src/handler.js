import axios from 'axios';

const API_URL = 'https://dn8mlk7hdujby.cloudfront.net/interview/insurance/policy';
// business rules
const EMPLOYEE_POLICY_COST = {
  noChildren: 0.279,
  oneChild: 0.4396,
  moreChildren: 0.5599,
};

const DENTAL_EMPLOYEE_POLICY_COST = {
  noChildren: 0.12,
  oneChild: 0.1950,
  moreChildren: 0.2480,
};

const calculateEmployeePolicyCost = (age, children, hasDentalCare = false) => {
  if (age > 65)
    return 0;
  if (children === 0)
    return EMPLOYEE_POLICY_COST.noChildren + (hasDentalCare ? DENTAL_EMPLOYEE_POLICY_COST.noChildren : 0);
  if (children === 1)
    return EMPLOYEE_POLICY_COST.oneChild + (hasDentalCare ? DENTAL_EMPLOYEE_POLICY_COST.oneChild : 0);
  return EMPLOYEE_POLICY_COST.moreChildren + (hasDentalCare ? DENTAL_EMPLOYEE_POLICY_COST.moreChildren : 0);
}

// utils functions
const roundNumber = number =>
  Math.round((number + Number.EPSILON) * 10000) / 10000;

// handler function
export default async (event, context) => {
  let response;
  try {
    response = await axios.get(API_URL);
    if (response && response.data && !response.data.success)
      throw { error: 'data source recovery doesn\'t indicate success' };
  } catch (e) {
    return {
      statusCode: 500,
      body: JSON.stringify(e),
    }
  }
  const { workers, has_dental_care, company_percentage } = response.data.policy;

  const employeeDetailsList = workers[0].map(worker => {
    const policyCost = calculateEmployeePolicyCost(worker.age, worker.childs, has_dental_care);
    const companyPayment = roundNumber(policyCost * (company_percentage / 100));
    const workerCopayment = roundNumber(policyCost - companyPayment);
    return {
      ...worker,
      policyCost,
      companyPayment,
      workerCopayment,
    };
  });

  const { totalPolicyCost, totalCompanyCost, totalEmployeeCost } = employeeDetailsList
    .reduce((acc, cost) => ({
      totalPolicyCost: acc.totalPolicyCost + cost.policyCost,
      totalCompanyCost: acc.totalCompanyCost + cost.companyPayment,
      totalEmployeeCost: acc.totalEmployeeCost + cost.workerCopayment,
    }), { totalPolicyCost: 0, totalCompanyCost: 0, totalEmployeeCost: 0 });

  return {
    statusCode: 200,
    body: JSON.stringify({
      employeeDetailsList,
      totalPolicyCost: roundNumber(totalPolicyCost),
      totalCompanyPayment: roundNumber(totalCompanyCost),
      totalEmployeeCopayment: roundNumber(totalEmployeeCost),
    }),
  };
};
