# Policy Calculator API

This API calculates the company payment of a colective insurance policy.

## Developer notes
I use NodeJS with Serverless framework in order to run the code as a service. As I am familiar with ES6 JS, it was used [serverless-webpack](https://github.com/serverless-heaven/serverless-webpack) plugin to enable babel translation from ES6 to compatible javascript version. Also, in order to enable service local running, I use 
[serverless-offline](https://github.com/dherault/serverless-offline) plugin.
Main code application is on `src/handler.js` file. `src/handlers.spec.js` is a unit test suite file for main code.

## Setup
### Prerequisites
- NodeJS
- Node Package Manager (npm)

### install
```bash
npm install
```

## Run service locally

```bash
npm start
```

## Unit test
```bash
npm test
```

## Usage

You can retrieve the policy value using the following command after service is running:

### GET /policy

```bash
curl -H "Content-Type:application/json" http://localhost:3000/policy
```

Example Result:
```bash
{"employeeDetailsList":[{"age":53,"childs":3,"policyCost":0.8079,"companyPayment":0.6463,"workerCopayment":0.1616},{"age":70,"childs":0,"policyCost":0,"companyPayment":0,"workerCopayment":0},{"age":34,"childs":2,"policyCost":0.8079,"companyPayment":0.6463,"workerCopayment":0.1616},{"age":53,"childs":3,"policyCost":0.8079,"companyPayment":0.6463,"workerCopayment":0.1616},{"age":52,"childs":2,"policyCost":0.8079,"companyPayment":0.6463,"workerCopayment":0.1616},{"age":64,"childs":1,"policyCost":0.6346,"companyPayment":0.5077,"workerCopayment":0.1269},{"age":55,"childs":2,"policyCost":0.8079,"companyPayment":0.6463,"workerCopayment":0.1616},{"age":54,"childs":3,"policyCost":0.8079,"companyPayment":0.6463,"workerCopayment":0.1616},{"age":67,"childs":3,"policyCost":0,"companyPayment":0,"workerCopayment":0},{"age":34,"childs":3,"policyCost":0.8079,"companyPayment":0.6463,"workerCopayment":0.1616},{"age":41,"childs":1,"policyCost":0.6346,"companyPayment":0.5077,"workerCopayment":0.1269},{"age":35,"childs":0,"policyCost":0.399,"companyPayment":0.3192,"workerCopayment":0.0798},{"age":63,"childs":3,"policyCost":0.8079,"companyPayment":0.6463,"workerCopayment":0.1616},{"age":39,"childs":4,"policyCost":0.8079,"companyPayment":0.6463,"workerCopayment":0.1616},{"age":39,"childs":0,"policyCost":0.399,"companyPayment":0.3192,"workerCopayment":0.0798}],"totalPolicyCost":9.3383,"totalCompanyPayment":7.4705,"totalEmployeeCopayment":1.8678}
```

## Pending
- Use ESLint to ensure code standarization.
- Extract policy cost values to an independent file in order to share it with test file.
- ...

## Thanks
As this is my first time using serverless, this [example](https://github.com/serverless/examples/tree/master/aws-node-rest-api-with-dynamodb-and-offline) was very useful to me. Also, I read/follow this [article](https://medium.com/@kilgarenone/write-es6-es7-in-serverless-framework-using-babel-7-and-webpack-4-5bd742168e1a) published by [Kheoh Yee Wei](https://medium.com/@kilgarenone) to enable ES6. It was a very simple explanation to follow.